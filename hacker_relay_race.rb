require 'net/http'
require 'oj'
require 'pry'
require 'date'

uri = URI('http://judging.thehackerolympics.com/leaderboard.json')
json_data = Oj.load(Net::HTTP.get(uri))
winners = json_data.map{|x| [x.last["points"], x.last["name"]]}.sort{|x,y| y <=> x}.take(3)
puts "If the Hacker Olympics Twilio competition ended at #{Time.now} here would be the winners:"
puts "1st: #{winners.first.last}"
puts "2nd: #{winners.at(1).last}"
puts "3rd: #{winners.last.last}"
